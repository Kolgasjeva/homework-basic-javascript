
const tabs = document.querySelectorAll(".tabs-title");
const paragraphs = document.querySelectorAll(".hidden");

tabs.forEach(tab => {
  tab.addEventListener("click", function () {

    const target = tab.dataset.target;
    const activeParagraph = document.querySelector(`.${target}`);

    paragraphs.forEach(paragraph => paragraph.classList.remove('block'));
    activeParagraph.classList.add('block')

    tabs.forEach(tab => tab.classList.remove('active'));
    tab.classList.add('active');
  })
})



