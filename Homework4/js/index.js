/*1) Описати своїми словами навіщо потрібні функції у програмуванні.
Функції у програмування необхідні для того, щоб не повторювати той самий код у багатьох місцях.
Функція реалізує певний алгоритм і дозволяє звернення до неї з різних частин програми.
2) Описати своїми словами, навіщо у функцію передавати аргумент.
Аргумент – це значення, передане в функцію під час її виклику (використовується під час виконання функції).
Ми оголошуємо функції, вказуючи їхні параметри, потім викликаємо їх, передаючи аргументи.
Аргумент передаємо у функція для того, щоб задати значення для параметрів функції.
3) Що таке оператор return та як він працює всередині функції?
Функція може повернути результат, який буде переданий у код, що її викликав за допомогою оператора return.
Оператор return може бути в будь-якому місці тіла функції. 
Як тільки виконання доходить до цього місця, функція зупиняється, і значення повертається в код, що викликав її.
Отже, оператор return завершує виконання поточної функції та повертає її значення.
*/

let firstNumber = "";

function getFirstNumber() {
    let stringFirstNumber = " ";
    let firstNumber = Number(stringFirstNumber);
    do {
        stringFirstNumber = prompt("Enter a  first number", stringFirstNumber === null ? "" : stringFirstNumber);
        firstNumber = Number(stringFirstNumber);
    } while (Number.isNaN(firstNumber) || stringFirstNumber === null || stringFirstNumber === "");
    return firstNumber;
}

let secondNumber = "";

function getSecondNumber() {
    let stringSecondNumber = " ";
    let secondNumber = Number(stringSecondNumber);
    do {
        stringSecondNumber = prompt("Enter a second number", stringSecondNumber === null ? "" : stringSecondNumber);
        secondNumber = Number(stringSecondNumber);
    } while (Number.isNaN(secondNumber) || stringSecondNumber === null || stringSecondNumber === "");
    return secondNumber;
}

let oper = "";

function getOper() {
    let oper = " ";
    do {
        oper = prompt("Enter a mathematical operation", oper === null ? "" : oper);
    } while (oper !== "+" && oper !== "-" && oper !== "/" && oper !== "*");
    return oper;
}

function executeOper(firstNumber, secondNumber, oper) {
    let numberFirst = getFirstNumber(firstNumber);
    let numberSecond = getSecondNumber(secondNumber);
    let operator = getOper(oper);

    switch (operator) {
        case "+":
            console.log(numberFirst + numberSecond);
            break;
        case "-":
            console.log(numberFirst - numberSecond);
            break;
        case "/":
            console.log(numberFirst / numberSecond);
            break;
        case "*":
            console.log(numberFirst * numberSecond);
            break;
        case "**":
            console.log(numberFirst ** numberSecond);
            break;
    }
}

executeOper(firstNumber, secondNumber, oper);


