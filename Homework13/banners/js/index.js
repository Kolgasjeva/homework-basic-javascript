/*1.Опишіть своїми словами різницю між функціями setTimeout() і setInterval()`.
Метод setTimeout запускається лише один раз за виклик, а значить після завершення функції setTimeout припиняє свою роботу.
Метод setInterval використовується для повторного виконання функції після закінчення певного періоду часу.
2. Що станеться, якщо в функцію setTimeout() передати нульову затримку? Чи спрацює вона миттєво і чому?
 Функція setTimeout() з нульовою затримкою планує виконання func якнайшвидше.
Але функція планувальник викликає його лише після завершення виконання поточного скрипту.
Таким чином, функція планується до запуску “відразу після” поточного скрипту.
3. Чому важливо не забувати викликати функцію clearInterval(), коли раніше створений цикл запуску вам вже не потрібен? 
Щоб припинити подальші виклики, нам слід викликати clearInterval. 
У більшості браузерів, включаючи Chrome та Firefox, внутрішній таймер продовжує “тікати” , якщо не  викликати clearInterval.
Поки ми не викличемо clearInterval, таймер тікає і це грузить браузер, йому необхідно буде оброблювати це, відповідно оброблювати буде і комп'ютер користувача, сторінка буде завантажуватися повільніше.
*/

let arraySrcImg = ["1.jpg", "2.jpg", "3.JPG", "4.png"];
let images = document.querySelectorAll('.image-to-show')
let count = 0;
let btnStop = document.querySelector('.btn-stop');
let btnRestart = document.querySelector('.btn-restart');

function changeImg() {
    images[count].style.display = "none";
    count++;
    if (count === images.length) {
        count = 0;
    }

    images[count].style.display = "block";
}

let timer = setInterval(changeImg, 3000);

btnStop.addEventListener('click', function () {
    clearInterval(timer)
}
)

btnRestart.addEventListener('click', function () {
    timer = setInterval(changeImg, 3000);
})

