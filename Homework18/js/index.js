function deepCloneObject(object) {


  if (typeof object === null || object !== "object") {
    return object;
  }

  let cloneObject = "";
  if (Array.isArray(object)) {
    cloneObject = [];
  } else {
    cloneObject = {};
  }

  for (let key in object) {
    if (typeof object === "object") {
      cloneObject[key] = cloneObject(object[key]);
    }
  }

  return cloneObject;
}

let deepClone = deepCloneObject(object);
console.log(deepClone)
