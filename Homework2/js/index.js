/*1) Які існують типи даних у Javascript?
Число, рядок, булевий (логічний), значення «null», значення «undefined», об'єкти та символи, BigInt.
2) У чому різниця між == і ===?
Оператор суворої рівності=== перевіряє рівність без приведення типів. 
Наприклад якщо а=6, а b="6", то перевірка повертає false. 
Суворе порівняння == перевіряє рівність з приведенням типів.
Наприклад якщо а=6, а b="6", то перевірка повертає true. b (рядок) оператором == перетворюється до числа.
3) Що таке оператор?
Оператор – це внутрішня функція JavaScript. 
При використанні того чи іншого оператора ми, по суті, просто запускаємо ту чи іншу вбудовану функцію, яка виконує певні дії і повертає результат.
*/

let firstname = " ";

do {
    firstname = prompt("Write your name.", firstname === null ? "" : firstname);
} while (isFinite(firstname) || firstname.length < 3);

let age = " ";
let ageToNumber= Number(age);

do {
    age=prompt("Enter your age", age === null ? "" : age);
    ageToNumber=Number(age);
}  while (Number.isNaN(ageToNumber) || ageToNumber < 0 || ageToNumber > 120 || age === null || age === "")

if (ageToNumber < 18) {
    alert("You are not allowed to visit this website");
} else if ( ageToNumber > 22) {
    alert(`Welcome ${firstname}`);
} else {
    let result = confirm("Are you sure you want to continue?"); 
    if (result === true) {
        alert(`Welcome ${firstname}`);
    } else {
        alert("You are not allowed to visit this website");
    }
}
