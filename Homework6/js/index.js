/*1)Опишіть своїми словами, що таке екранування, і навіщо воно потрібне в мовах програмування.
Екранування символів — заміна в тексті керуючих символів на відповідні текстові підстановки.
Наприклад, щоб ми могли представити кілька лапок у рядку без того, щоб JavaScript неправильно інтерпретував те, і сприймав екрановані лапки як символ для виведення, а не як кінець поточного рядка.
Лапки - не єдині символи, які можна екранувати всередині рядка.
2)Які засоби оголошення функцій ви знаєте?
Оголошення (декларація) функцій. Спочатку ми пишемо function — це ключове слово (keyword), яке дає зрозуміти комп’ютеру, що далі буде оголошення функції.  Потім — назву функції і список її параметрів в дужках. І нарешті, код функції, який також називають тілом функції між фігурними дужками.
Function expression - функція також оголошується за допомогою ключового слова function, але вона не має імені, і вона записується в змінну.
Named Function expression - функція записується в змінну, а також має своє ім'я (правда функцію не можна буде викликати по цьому імені).
Існує ще більш простий та короткий синтаксис для створення функцій, він називається "функції-стрілки" або "стрілочні функції".
Рекурсія, що означає виклик функцією себе.
Колбэк функція (від англійської callback function) – це звичайна функція, яка просто викликається всередині іншої функції.
3)Що таке hoisting, як він працює для змінних та функцій?
Hoisting - це механізм JavaScript, в якому змінні і оголошення функцій, пересуваються вгору своєї області видимості перед тим, як код буде виконано.
Як наслідок, це означає те, що зовсім неважливо де були оголошені функція або змінні, всі вони пересуваються вгору своєї області видимості.
*/

function createNewUser() {
    let firstName = prompt("Enter your firstname.");
    let lastName = prompt("Enter your lastname.");
    let birthday = prompt("Enter your date of birth in the format dd.mm.yyyy.");

    let newUser = {
        firstName: firstName,
        lastName: lastName,
        birthday: birthday,
        getLogin() {
            let login = this.firstName.toLowerCase()[0] + this.lastName.toLowerCase()
            return login
        },
        getAge() {
            let nowDate = new Date();
            let today = new Date(nowDate.getFullYear(), nowDate.getMonth(), nowDate.getDate());
            let birthDate = new Date(this.birthday.slice(6), this.birthday.slice(3, 5) - 1, this.birthday.slice(0, 2));
            let ageMs = today.getTime() - birthDate.getTime();
            let age = Math.floor(ageMs / (365.25 * 24 * 60 * 60 * 1000));
            return age;
            },

            getPassword () {
            let password  = this.firstName.toUpperCase()[0] + this.lastName.toLowerCase()+this.birthday.slice(6);
            return password
            }

        }
    

    Object.defineProperty(newUser, 'firstName', {
        value: firstName,
        configurable: true,
        writable: false
    })

    Object.defineProperty(newUser, 'lastName', {
        value: lastName,
        configurable: true,
        writable: false
    })

    newUser.setFirstName = function (nameFirst) {
        Object.defineProperty(newUser, 'firstName', {
            value: nameFirst
        })
    }

    newUser.setLastName = function (nameLast) {
        Object.defineProperty(newUser, 'lastName', {
            value: nameLast
        })
    }

    return newUser;

}

const user1 = createNewUser();
console.log(user1);
console.log(user1.getAge());
console.log(user1.getPassword());



