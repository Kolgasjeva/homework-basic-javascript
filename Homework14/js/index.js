const logos = document.querySelector('.header-container-logo-link');
const logoIcon = document.querySelector('.header-container-logo-img');
const logoIconOrange = document.querySelector('.header-logo-img-orange');
const links = document.querySelectorAll('.header-container-contacts-link')
const titles = document.querySelectorAll('.common-section-title')
const copyright = document.querySelector('.page-footer-copyright ');

function changeTheme() {
    let savedTheme = localStorage.getItem('theme');
    if (savedTheme === 'light') {
        localStorage.removeItem('theme');
        localStorage.setItem('theme', 'dark');
        document.body.classList.remove('light');
        logos.classList.remove('light');
        logoIcon.classList.remove('light');
        logoIconOrange.classList.remove('light');
        links.forEach(function (link) {
            link.classList.remove('light')
        })
        titles.forEach(function (title) {
            title.classList.remove('light')
        })

        copyright.classList.remove('light')
    } else {
        localStorage.removeItem('theme');
        localStorage.setItem('theme', 'light');
        document.body.classList.add('light');
        logos.classList.add('light');
        logoIcon.classList.add('light');
        logoIconOrange.classList.add('light');
        links.forEach(function (link) {
            link.classList.add('light');
        })
        titles.forEach(function (title) {
            title.classList.add('light');
        })
        copyright.classList.add('light')
    }
}

document.querySelector('.changeTheme').addEventListener('click', changeTheme)
savedTheme = localStorage.getItem('theme');

if (savedTheme === 'light') {
    document.body.classList.add('light');
    logos.classList.add('light');
    logoIcon.classList.add('light');
    logoIconOrange.classList.add('light');
    links.forEach(function (link) {
        link.classList.add('light');
    })
    titles.forEach(function (title) {
        title.classList.add('light');
    })
    copyright.classList.add('light')
} else {
    document.body.classList.remove('light');
    logos.classList.remove('light');
    logoIcon.classList.remove('light');
    logoIconOrange.classList.remove('light');
    links.forEach(function (link) {
        link.classList.remove('light')
    })
    titles.forEach(function (title) {
        title.classList.remove('light')
    })
    copyright.classList.remove('light')
}

