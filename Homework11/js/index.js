const form = document.querySelector('.password-form');
const firsTry = document.querySelector('.firsTry');
const secondTry = document.querySelector('.secondTry')

form.addEventListener("click", function (event) {
  if (event.target.tagName == 'I') {
    const target = event.target;
    const targetDataAttribute = target.dataset.target;
    const input = document.querySelector(`.${targetDataAttribute}`);
    if (target.classList.contains('fa-eye')) {
      target.classList.replace('fa-eye', 'fa-eye-slash')
      target.style.display = "block";
      input.type = "text";
    } else {
      target.classList.replace('fa-eye-slash', 'fa-eye')
      input.type = "password";
    }

  }
})

form.addEventListener("submit", function (event) {
  event.preventDefault();
  const errorMessage = document.createElement('p');
  errorMessage.textContent = 'Потрібно ввести однакові значення';
  errorMessage.style.color = 'red';
  if (firsTry.value !== secondTry.value) {
    const addErrorMessage = document.querySelector(".second-wrapper");
    addErrorMessage.append(errorMessage);
  }
  if (firsTry.value === secondTry.value) {
    document.querySelector(".second-wrapper").lastChild.remove();
    alert("You are welcome");
  }
})
