/*
1)Опишіть, як можна створити новий HTML тег на сторінці.
Створити новий HTML можна за допомогою метода document.createElement(tag). Потім необхідно вставити його за допомогою:
node.append(...nodes or strings)
node.prepend(...nodes or strings)
node.before(...nodes or strings)
node.after(...nodes or strings)
2) Опишіть, що означає перший параметр функції insertAdjacentHTML і опишіть можливі варіанти цього параметра.
Перший параметр - це спеціальне слово, що вказує, куди по відношенню до elem робити вставку. Значення має бути одним із наступних:
`beforebegin' - вставити html безпосередньо перед elem,
`afterbegin' - вставити html на початок elem,
`beforeend' - вставити html в кінець elem,
`afterend' - вставити html безпосередньо після elem.
3) Як можна видалити елемент зі сторінки?
Для видалення елемента є метод node.remove()
*/


function createList(array, parentElem = document.body) {
    let parentElement = "";
    if (typeof parentElem === 'string') {
        parentElement = document.createElement(parentElem)
        document.body.prepend(parentElement);
    } else parentElement = parentElem;

    let list = document.createElement('ul');
    parentElement.prepend(list);

    array.forEach((element) => {
        let item = document.createElement('li');
        item.innerHTML = element;
        list.prepend(item);
    });
}

createList(["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"], "p");
createList(["1", "2", "3", "sea", "user", 23]);



