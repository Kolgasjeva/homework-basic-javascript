/* 1. Чому для роботи з input не рекомендується використовувати клавіатуру?
На сучасних пристроях є інші способи «ввести щось». 
Наприклад, розпізнавання мови (це особливо актуально на мобільних пристроях) або Копіювати/Вставити за допомогою миші.
Тому, якщо хочемо коректно відстежувати введення у полі <input>, то одних клавіатурних подій недостатньо. 
Існує спеціальна подія input, щоб відстежувати будь-які зміни в полі <input>. 
І воно справляється з таким завданням набагато краще.
 */
const buttons = document.getElementsByClassName('btn');
let activeButton = null;

document.addEventListener("keydown", function (event) {
    const pressedButton = event.key.toUpperCase();

    for (let btn of buttons) {
        if (btn.textContent.toUpperCase() === pressedButton) {
            if (activeButton !== null && activeButton !== btn) {
                activeButton.classList.remove('active');
            }
            btn.classList.add('active');
            activeButton = btn;
        }
    }
})
