function countDeadline(arrayTeamSpeed, arrayBackLog, deadline) {
  const teamSpeed = arrayTeamSpeed.reduce((prevValue, currentValue) => prevValue + currentValue);
  const backLog = arrayBackLog.reduce((prevValue, currentValue) => prevValue + currentValue);
  const today = new Date();

  let workDays = 0;

  while (today < deadline) {
    if (today.getDay(today) !== 0 && today.getDay(today) !== 6) {
      workDays = workDays + 1;
    }
    today.setDate(today.getDate() + 1);
  }

  const totalDays = Math.ceil(backLog / teamSpeed)
  const diffDates = totalDays - workDays;
  const totalHours = Math.ceil((backLog / teamSpeed) * 8);
  const hoursDiffDeadline = (totalHours - (workDays * 8));


  if (totalDays < workDays) {
    alert(`Усі завдання будуть успішно виконані за ${diffDates * (-1)
      } днів до настання дедлайну!`)
  } else {
    alert(`Команді розробників доведеться витратити додатково ${hoursDiffDeadline} годин після дедлайну, щоб виконати всі завдання в беклозі`)
  }
}
