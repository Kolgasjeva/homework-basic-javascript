/*1) Опишіть своїми словами що таке Document Object Model (DOM)
Document Object Model (DOM) - програмний інтерфейс, що дозволяє програмам і скриптам отримати доступ до вмісту HTML, а також змінювати вміст, структуру та оформлення таких документів.
2)Яка різниця між властивостями HTML-елементів innerHTML та innerText?
Властивість innerHTML дозволяє отримати HTML-вміст елемента у вигляді рядка.
Отже, innerHTML візьме рядок HTML і відтворить його як звичайний HTML.
InnerText з іншого боку візьме звичайний рядок і відобразить його таким, яким він є. 
Він не відображатиме жодного HTML, як innerHTML.
Наприклад 
const titleEl = document.createElement("span");
titleEl.innerHTML = '<span style="color:orange;">techukraine.net</span>';
document.body.append(titleEl);
На сторінку виведе "techukraine.net"
const titleE2 = document.createElement("span");
titleE2.innerText=`<span style="color:orange;">techukraine.net</span>`;
document.body.prepend(titleE2)
нНа сторінку виведе `<span style="color:orange;">techukraine.net</span>`
3)Як можна звернутися до елемента сторінки за допомогою JS? Який спосіб кращий? 
Є декаліка способів звернутися до елемента сторінки за допомогою JS, а саме:
document.getElementById 
getElementsByTagName
getElementsByClassName
getElementsByName
querySelector
querySelectorAll
На мою думку, спосіб querySelectorAll найкращий, так як він є універсальним і за допомогою нього можно звернутися до будь-якого елемента на сторінці.
*/
const paragraphElem = document.querySelectorAll('p');

for ( let paragraph of paragraphElem) {
    paragraph.style.backgroundColor = '#ff0000 ';
}
const elementById = document.getElementById('optionsList');
console.log(elementById);

let parentElem = elementById.parentElement;
console.log(parentElem);

let nodeElem = elementById.childNodes;
for (let node of nodeElem) {
    console.log( `Назва ноди: ${node.nodeName}, тип ноди: ${node.nodeType}`)
}

const idElem = document.querySelector('#testParagraph');
idElem.innerText = 'This is a paragraph';

const classElem = document.querySelector(".main-header").children;
for ( let child of classElem) {
    console.log(child);
    child.classList.add('nav-item');
}

const sectionTitle = document.querySelectorAll('.section-title');
for (let element of sectionTitle) {
    element.classList.remove('section-title')
}

