/*1)Опишіть своїми словами, що таке метод об'єкту
Значенням властивості може бути функція — таку властивість називають методом. 
Методом називається функція, яка є членом об'єкта.
2)Який тип даних може мати значення властивості об'єкта?
 Властивість є значення або набір значень (у вигляді масиву або об'єкта), який є членом об'єкта і може містити будь який тип даних.
3) Об'єкт це посилальний тип даних. Що означає це поняття?
Однією з принципових відмінностей об’єктів від строк, чисел, логічних значень є те, що об’єкти зберігаються та копіюються “за посиланням”, тоді як рядки, числа, логічні значення тощо – завжди копіюються “за значенням”.
Змінна зберігає не сам об’єкт, а його “адресу в пам’яті” – іншими словами “посилання” на нього.
Коли копіюється змінна об’єкта, копіюється посилання, але сам об’єкт не дублюється.
*/

function createNewUser() {
    let firstName = prompt("Enter your firstname.");
    let lastName = prompt("Enter your lastname.");
    let newUser = {
        firstName: firstName,
        lastName: lastName,
        getLogin() {
            let login =this.firstName.toLowerCase()[0] + this.lastName.toLowerCase()
            return login 
        }
    }

    Object.defineProperty(newUser, 'firstName', {
        value: firstName,
        configurable: true,
        writable: false
    })
    
    Object.defineProperty(newUser, 'lastName', {
        value: lastName,
        configurable: true,
        writable: false
    })

    newUser.setFirstName = function (nameFirst) {
        Object.defineProperty(newUser, 'firstName', {
            value: nameFirst
        })
    }

    newUser.setLastName = function (nameLast) {
        Object.defineProperty(newUser, 'lastName', {
            value: nameLast
        })
    }

    return newUser;

}

const user1 = createNewUser();
console.log(user1);
console.log(user1.getLogin());




