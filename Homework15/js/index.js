/*У процесі виконання завдань у функціях можуть бути викликані інші функції для виконання підзадач. 
 Рекурсією називають процес, коли функція викликає сама себе. 
 Тобто, оголошується функцію і в її тілі відбувається звернення до неї (вона викликається).
 Рекурсія корисна у ситуаціях, коли завдання може бути природно розділена на кілька аналогічних, але більш простих завдань. 
 Або коли завдання може бути спрощено до нескладних дій плюс простий варіант тієї самої задачі.
*/

function getNumber() {
    let stringFirstNumber = " ";
    let firstNumber = Number(stringFirstNumber);
    do {
        stringFirstNumber = prompt("Enter a  first number", stringFirstNumber === null ? "" : stringFirstNumber);
        firstNumber = Number(stringFirstNumber);
    } while (Number.isNaN(firstNumber) || stringFirstNumber === null || stringFirstNumber === "");
    return firstNumber;
}

let numberFirst = getNumber();

let factorial = (numberFirst) => {
    if (numberFirst < 0) {
        return mistake = "Факторіали для таких чисел не визначені";
    } else if (numberFirst === 1 || numberFirst === 0) {
        return 1;
    } else {
        return numberFirst * factorial(numberFirst - 1);
    }
}

let answer = factorial(numberFirst);
console.log(answer);
